import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/Authentication/Login/login.component';
import { HomeComponent } from './Components/Home/home.component';

import { AuthService } from "./Services/auth.service";
import { AuthGuard } from "./Services/auth.guard";

import { LoggerModule, NgxLoggerLevel } from "ngx-logger";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { JwtInterceptor } from "./Helpers/jwt.interceptor";
import { ReactiveFormsModule } from "@angular/forms";
import { NavbarComponent } from './Components/Navbar/navbar.component';
import { StaffListComponent } from './Components/Staff/staff-list/staff-list.component';
import { StaffFormComponent } from './Components/Staff/staff-form/staff-form.component';
import { StaffService } from "./Services/staff.service";

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RippleModule } from "primeng/ripple";
import { StaffPageComponent } from './Components/Staff/staff-page/staff-page.component';
import { LoadingScreenComponent } from './Components/LoadingScreen/loading-screen.component';
import { LoadingScreenInterceptor } from "./Helpers/loading-screen.interceptor";
import { ToastModule } from 'primeng/toast';
import { ErrorService } from "./Services/error.service";

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        NavbarComponent,
        StaffListComponent,
        StaffFormComponent,
        StaffPageComponent,
        LoadingScreenComponent
    ],
    imports: [
        // HttpClientModule is only needed if you want to log on server or if you want to inspect sourcemaps
        HttpClientModule,
        LoggerModule.forRoot({
            serverLoggingUrl: '/api/logs',
            level: NgxLoggerLevel.DEBUG,
            serverLogLevel: NgxLoggerLevel.ERROR
        }),
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        TableModule,
        ButtonModule,
        DialogModule,
        ConfirmDialogModule,
        RippleModule,
        ToastModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoadingScreenInterceptor,
            multi: true
        },
        AuthService,
        AuthGuard,
        StaffService,
        ConfirmationService,
        MessageService,
        ErrorService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
