import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { UserModel } from "../../../Models/UserModel";
import { ConfirmationService, LazyLoadEvent } from "primeng/api";
import { StaffService } from "../../../Services/staff.service";
import { UserListFilterModel } from "../../../Models/UserListFilterModel";

@Component({
    selector: 'app-staff-list',
    templateUrl: './staff-list.component.html',
    styleUrls: ['./staff-list.component.scss']
})
export class StaffListComponent implements OnInit {
    @Output() deleteEvent = new EventEmitter<UserModel>();
    @Output() editEvent = new EventEmitter<UserModel>();
    @Output() newEvent = new EventEmitter();

    selectedStaff: UserModel = new UserModel();
    staff: UserModel[] = [];

    rows = 2;
    isLoading = true;
    totalRecords: number = 0;

    filterParams: UserListFilterModel = new UserListFilterModel({
        PageIndex: 0,
        Limit: this.rows
    })

    constructor(
        private confirmationService: ConfirmationService,
        private staffService: StaffService
    ) {
    }

    // Get All staff from backend
    getAllStaff(params: UserListFilterModel) {
        this.staffService.getAllStaff(params).subscribe((staff: UserModel[]) => {
            this.staff = staff;
            this.isLoading = false;
        })
    }

    ngOnInit(): void {
        this.isLoading = true;

        this.staffService.getAllStaff().subscribe((staff: UserModel[]) => {
            this.totalRecords = staff.length;

            this.staffService.getAllStaff(this.filterParams).subscribe((staff: UserModel[]) => {
                this.staff = staff;
                this.isLoading = false;
            })
        });
    }

    // On pagination event
    onPage(event: any) {
        this.isLoading = true;
        this.filterParams.PageIndex = event.first / event.rows;
        this.filterParams.Limit = event.rows;

        this.getAllStaff(this.filterParams);
    }

    // On sort event
    onSort(event: any) {
        this.filterParams.OrderBy = event.field;
        this.filterParams.Order = event.order === 1 ? 'Asc' : 'Desc';
        this.getAllStaff(this.filterParams);
    }

    // Emit new event
    new() {
        this.newEvent.emit();
    }

    // Emit edit event
    edit(event: any) {
        this.editEvent.emit(event.data);
    }

    // Emit delete event and open confirmation popup
    delete(event: any) {
        this.confirmationService.confirm({
            message: 'A törlés gombra kattintva a munkatárs kitörlődik a rendszerből!',
            header: 'Biztosan törli a munkatársat?',
            icon: 'pi pi-exclamation-triangle',
            acceptLabel: 'Törlés',
            rejectLabel: 'Vissza',
            acceptButtonStyleClass: 'p-button-primary',
            rejectButtonStyleClass: 'p-button-outlined',
            accept: () => {
                this.deleteEvent.emit(event);
                this.staff = this.staff.filter(val => val.id !== event.id);
            }
        });
    }
}
