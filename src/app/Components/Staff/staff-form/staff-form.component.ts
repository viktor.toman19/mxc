import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserDtoModel } from "../../../Models/UserDtoModel";
import { StaffService } from "../../../Services/staff.service";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { UserModel } from "../../../Models/UserModel";
import { ErrorService } from "../../../Services/error.service";
import { PasswordValidator } from "../../../Helpers/password.validator";

@Component({
    selector: 'app-staff-form',
    templateUrl: './staff-form.component.html',
    styleUrls: ['./staff-form.component.scss']
})
export class StaffFormComponent implements OnInit {

    @Input() method: string = '';
    @Input() editData: UserDtoModel = new UserDtoModel();
    @Output() backEvent = new EventEmitter<string>();

    // Add required validators and add pattern has to contain email and password:
    // - At least 5 characters in length
    // - Lowercase letters
    // - Uppercase letters
    // - Numbers
    form = this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        userName: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(5), PasswordValidator.strong]],
        email: ['', Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")],
        phoneNumber: [''],
    });

    staff: UserModel[] = [];

    constructor(
        private fb: FormBuilder,
        private staffService: StaffService,
        public router: Router,
        private messageService: MessageService,
        public errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {}

    ngOnChanges(): void {
        this.setFormData();
    }

    // Set form data
    private setFormData() {
        this.form.controls['firstName'].setValue(this.method === 'create' ? '' : this.editData.firstName);
        this.form.controls['lastName'].setValue(this.method === 'create' ? '' : this.editData.lastName);
        this.form.controls['userName'].setValue(this.method === 'create' ? '' : this.editData.userName);
        this.form.controls['phoneNumber'].setValue(this.method === 'create' ? '' : this.editData.phoneNumber);
        this.form.controls['email'].setValue(this.method === 'create' ? '' : this.editData.email);
    }

    // Get Form data
    private getFormData() {
        return this.form.value;
    }

    // Save and update function
    save() {
        // If the form is invalid return an error
        if (this.form.invalid) {
            this.messageService.add({severity:'error', summary:'Sikertelen mentés.', detail:'Minden csillaggal (*) megjelölt mező kitöltése kötelező.'});
            return;
        }

        // Separate save or update method
        if (this.method === 'create') {
            this.staffService.createStaff(this.getFormData()).subscribe((response: any) => {
                if (response === undefined) {
                    this.messageService.add({severity:'error', summary:'Sikertelen művelet.', detail:this.errorService.getError()});
                } else {
                    this.messageService.add({severity:'success', summary:'Sikeres mentés.', detail:'A munkatárs létrehozása sikeres volt.'});
                    this.back();
                }
            })
        } else {
            this.staffService.updateStaff(this.editData.id, this.getFormData()).subscribe((response: any) => {
                if (response === undefined) {
                    this.messageService.add({severity:'error', summary:'Sikertelen művelet.', detail:this.errorService.getError()});
                } else {
                    this.messageService.add({severity:'success', summary:'Sikeres mentés.', detail:'A munkatárs adatainak módosítása sikeres volt.'});
                    this.back();
                }
            })
        }
    }

    // Back function
    back() {
        this.method = 'list';
        this.backEvent.emit(this.method);
    }
}
