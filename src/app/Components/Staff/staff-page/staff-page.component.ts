import { Component, OnInit } from '@angular/core';
import { UserModel } from "../../../Models/UserModel";
import { StaffService } from "../../../Services/staff.service";
import { UserDtoModel } from "../../../Models/UserDtoModel";
import { MessageService } from "primeng/api";

@Component({
    selector: 'app-staff-page',
    templateUrl: './staff-page.component.html',
    styleUrls: ['./staff-page.component.scss']
})
export class StaffPageComponent implements OnInit {

    method: string = 'list';
    editData: UserModel = new UserModel();
    user: UserDtoModel = new UserDtoModel();
    staff: UserModel[] = [];

    constructor(
        private staffService: StaffService,
        private messageService: MessageService
    ) {
    }

    ngOnInit(): void {}

    // Set back method
    back(method: string) {
        this.method = method;
    }

    // Set new method
    new() {
        this.method = 'create';
    }

    // Set edit method and get selected user data
    edit(data: UserModel) {
        this.method = 'edit';
        this.editData = new UserModel(data);

        if (this.editData) {
            const id = this.editData.id || '';

            this.staffService.getStaff(id).subscribe((staff: UserDtoModel) => {
                this.user = new UserDtoModel(staff);
            })
        }
    }

    // Delete selected user
    delete(data: UserModel) {
        const id = data.id || '';

        this.staffService.deleteStaff(id).subscribe((response: any) => {
            if (response === 200) {
                this.messageService.add({severity:'success', summary:'Sikeres törlés.', detail:'A munkatárs törlése sikeres volt.'});
            } else {
                this.messageService.add({severity:'error', summary:'Sikertelen törlés.', detail:'A munkatárs törlése sikertelen volt.'});
            }
        });
    }
}
