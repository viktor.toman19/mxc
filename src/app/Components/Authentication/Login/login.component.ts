import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../../Services/auth.service";
import { Router } from "@angular/router";
import { ErrorService } from "../../../Services/error.service";
import { MessageService } from "primeng/api";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    signInForm: FormGroup;
    isLoading = false;

    constructor(
        public fb: FormBuilder,
        public authService: AuthService,
        public errorService: ErrorService,
        public router: Router,
        private messageService: MessageService
    ) {
        this.signInForm = this.fb.group({
            userName: ['', Validators.required],
            password: ['', Validators.required]
        })
    }

    ngOnInit(): void {
        // If the user is logged in navigate to the staff list
        if (this.authService.isLoggedIn) {
            this.router.navigate([''])
        }
    }

    // Login user
    loginUser() {
        this.isLoading = true;
        this.authService.login(this.signInForm.value).subscribe((response: any) => {
            if (response === undefined) {
                this.messageService.add({severity:'error', summary:'Sikertelen művelet.', detail:this.errorService.getError()});
            }
        })
    }
}
