import { Component, Input, OnInit } from '@angular/core';
import { UserDtoModel } from "../../Models/UserDtoModel";
import { AuthService } from "../../Services/auth.service";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    @Input() user: UserDtoModel = new UserDtoModel();

    constructor(
        public authService: AuthService
    ) {
    }

    ngOnInit(): void {
    }

    // Call logout function
    logout() {
        this.authService.logout();
    }
}
