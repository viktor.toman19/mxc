import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../Services/auth.service";
import { UserDtoModel } from "../../Models/UserDtoModel";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    user: UserDtoModel = new UserDtoModel();

    constructor(
        public authService: AuthService,
    ) {
    }

    ngOnInit(): void {
        this.user = new UserDtoModel(this.authService.getUser());
    }
}
