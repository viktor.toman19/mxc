import { Component } from '@angular/core';
import { AuthService } from "./Services/auth.service";
import { isJwtExpired } from 'jwt-check-expiration';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'mxc';

    constructor(
        public authService: AuthService,
    ) {
        //If token is expired logout the user
        if (this.authService.getToken()) {
            if (isJwtExpired(this.authService.getToken())) {
                this.authService.logout();
            }
        }
    }
}
