export class UserDtoModel implements IUserDtoModel {
    id!: string;
    firstName!: string;
	lastName!: string;
	email!: string;
	userName!: string;
	// @ts-ignore
    phoneNumber?: string;
    organisationsWithRole?: any[];

    constructor(data?: IUserDtoModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IUserDtoModel {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    userName: string;
    // @ts-ignore
    phoneNumber?: string;
    organisationsWithRole?: any[];
}
