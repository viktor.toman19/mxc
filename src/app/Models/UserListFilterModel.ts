export class UserListFilterModel implements IUserListFilter {
    Order?: string;
    OrderBy?: string;
    UserName?: string;
    Limit?: number;
    PageIndex?: number;

    constructor(data?: IUserListFilter) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IUserListFilter {
    Order?: string;
    OrderBy?: string;
    UserName?: string;
    Limit?: number;
    PageIndex?: number;
}

export enum OrderDirection {
    ASC = 1,
    DESC = -1
}
