export class UserModel implements IUser {
    firstName!: string;
	lastName!: string;
	email!: string;
	userName!: string;
	// @ts-ignore
    createdAt?: Date;
	id?: string;

    constructor(data?: IUser) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }

    get getFullName(): string {
        return this.firstName ? `${this.firstName} ${this.lastName}` : '';
    }
}

export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    userName: string;
    // @ts-ignore
    createdAt?: Date;
    id?: string;
}
