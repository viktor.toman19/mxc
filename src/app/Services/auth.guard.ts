import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from "./auth.service";
import { NGXLogger } from "ngx-logger";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router,
        private logger: NGXLogger
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // If the user is not logged in navigate to login page
        if (!this.authService.isLoggedIn) {
            this.router.navigate(['login'])
        }

        return true;
    }

}
