import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { UserModel } from "../Models/UserModel";
import { environment } from "../../environments/environment";
import { catchError, Observable, tap, throwError } from 'rxjs';
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { UserDtoModel } from "../Models/UserDtoModel";
import { ErrorService } from "./error.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    apiUrl: string = environment.apiUrl;
    currentUser:any = {};
    errorText: string = '';

    constructor(
        private http: HttpClient,
        public router: Router,
        private error: ErrorService
    ) {}

    // Check the User is authenticated
    get isLoggedIn(): boolean {
        let authToken = localStorage.getItem('token');
        return (authToken !== null);
    }

    // Login user
    login(user: UserModel) {
        return this.http.post<any>(`${this.apiUrl}identity/token`, user)
            .pipe(
                tap(
                    (res: any) => {
                        localStorage.setItem('token', res.access_token);

                        //Get user data if is logged in and store data in localstorage
                        this.getUserProfile().subscribe((user: UserDtoModel) => {
                            localStorage.setItem('user', JSON.stringify(user))
                            this.router.navigate(['']);
                        })
                    }
                ),
                catchError(this.error.handleError<UserModel>('login'))
            );
    }

    // User profile
    getUserProfile(): Observable<UserDtoModel> {
        return this.http.get(`${this.apiUrl}user/me`).pipe(
            map((res: any) => {
                return res
            }),
            catchError(this.error.handleError<UserDtoModel>('getUserProfile'))
        )
    }

    // Get parsed user object
    getUser() {
        if (localStorage.getItem('user') !== null) {
            return JSON.parse(localStorage.getItem('user') || '');
        }

        return '';
    }

    // Get stored token
    getToken() {
        return localStorage.getItem('token') || '';
    }

    // Logout function
    logout() {
        let httpOptions = {
            headers: new HttpHeaders({
                'X-OrganisationId': this.getUser().organisationsWithRole[0].organisationId
            })
        }

        this.http.post(`${this.apiUrl}identity/logout`, {}, httpOptions).subscribe((logout) => {
            localStorage.removeItem('token');
            this.router.navigate(['login']);
        })
    }
}
