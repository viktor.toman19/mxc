import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { ErrorMessageModel } from "../Models/ErrorMessageModel";
import { ErrorModel } from "../Models/ErrorModel";

@Injectable({
    providedIn: 'root'
})
export class ErrorService {
    errorMsg: string = '';

    constructor() {}

    handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(error.error.functionCode)
            this.errorMsg = this.findErrorMsg(error.error);
            return of(result as T);
        };
    }

    // Get error message
    getError() {
        return this.errorMsg;
    }

    // Find error message by error code
    findErrorMsg(errorCode: ErrorModel) {
        let msg: string = '';
        let messages: ErrorMessageModel[] = errorCode.messages;

        console.log(errorCode);
        console.log(messages);

        switch (errorCode.functionCode) {
            case 'API_ISFTICKETING_V1_USERCANNOTSIGNINTOKENREQUEST':
                msg = 'Helytelen felhasználónév vagy jelszó!';
                break;
            case 'API_ISFTICKETING_V1_USERNOTFOUNDTOKENREQUEST':
                msg = 'Nem létező felhasználó!';
                break;
            case 'API_ISFTICKETING_V1_REGISTERCANNOTCOMPLETE':
                if (messages.length > 0) {
                    messages.map((message: ErrorMessageModel) => {
                        msg = this.findErrorMessageByLabel(message.label);
                    })
                } else {
                    msg = 'A munkatárs mentése sikertelen!';
                }
                break;
            default:
                msg = 'Ismeretlen hiba!';
                break;
        }

        return msg;
    }

    // Find error message from messages array
    findErrorMessageByLabel(label: string) {
        let msg: string = '';

        switch (label) {
            case 'PasswordTooShort':
                msg = 'A megadott jelszó túl rövid!';
                break;
            case 'DuplicateUserName':
                msg = 'A megadott felhasználónév már létezik!';
                break;
            default:
                msg = 'Ismeretlen hiba!';
                break;
        }

        return msg;
    }
}
