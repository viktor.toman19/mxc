import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, Observable, tap } from "rxjs";
import { map } from "rxjs/operators";
import { ErrorService } from "./error.service";
import { UserModel } from "../Models/UserModel";
import { AuthService } from "./auth.service";
import { UserDtoModel } from "../Models/UserDtoModel";
import { MessageService } from "primeng/api";
import { UserListFilterModel } from "../Models/UserListFilterModel";
import { Router, UrlSerializer } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class StaffService {

    apiUrl: string = `${environment.apiUrl}admin/user`;
    httpOptions: any = {};

    constructor(
        private http: HttpClient,
        private error: ErrorService,
        private authService: AuthService,
        private router: Router,
        private serializer: UrlSerializer
    ) {
        const user = this.authService.getUser();

        this.httpOptions = {
            headers: new HttpHeaders({
                'X-OrganisationId': user.organisationsWithRole[0].organisationId
            })
        }
    }

    // Get all staff from backend
    getAllStaff(filterParams?: UserListFilterModel): Observable<UserModel[]> {
        const queryParams = this.router.createUrlTree([], { queryParams: filterParams });

        return this.http.get(this.apiUrl + this.serializer.serialize(queryParams), this.httpOptions).pipe(
            map((res: any) => {
                let body:any = {};

                if (res.body !== undefined) {
                    body = JSON.parse(res.body);
                }

                return res.results !== undefined ? res.results : body.results;
            }),
            catchError(this.error.handleError<UserModel>('getAllStaff')),
            map(dtoList => dtoList.map(dto => new UserModel(dto)))
        )
    }

    // Get one staff from backend
    getStaff(id: string): Observable<UserDtoModel> {
        return this.http.get(`${this.apiUrl}/${id}`, this.httpOptions).pipe(
            map((res: any) => {
                let body:any = {};

                if (res.body !== undefined) {
                    body = JSON.parse(res.body);
                }

                return res.body !== undefined ? body : res;
            }),
            catchError(this.error.handleError<UserDtoModel>('getStaff'))
        )
    }

    // Create new staff on backend
    createStaff(data: UserDtoModel): Observable<any> {
        return this.http.post<any>(this.apiUrl, data, this.httpOptions)
            .pipe(
                tap(
                    (res: any) => {
                        return res;
                    }
                ),
                catchError(this.error.handleError<UserDtoModel>('createStaff'))
            );
    }

    // Update an existing staff on backend
    updateStaff(id: string, data: UserDtoModel): Observable<any> {
        return this.http.put<any>(`${this.apiUrl}/${id}`, data, this.httpOptions)
            .pipe(
                tap(
                    (res: any) => {
                        return res;
                    }
                ),
                catchError(this.error.handleError<UserDtoModel>('updateStaff'))
            );
    }

    // Delete on existing staff on backend
    deleteStaff(id: string) {
        this.httpOptions.responseType = 'text';
        this.httpOptions.observe = 'response';

        return this.http.delete(`${this.apiUrl}/${id}`, this.httpOptions).pipe(
            map((response: any) => {
                return response.status
            }),
            catchError(this.error.handleError<any>('deleteStaff'))
        );
    }
}
